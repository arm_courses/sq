<?xml version="1.0" encoding="UTF-8"?>

<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.arm.crse.sq</groupId>
    <artifactId>testing-parent</artifactId>
    <packaging>pom</packaging>
    <version>1.0-SNAPSHOT</version>
    <modules>
        <module>dynamic</module>
        <module>static</module>
    </modules>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <project.current.version>1.0-SNAPSHOT</project.current.version>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>

        <!--#region dependency test-->
        <dependency.junit-bom.version>5.8.2</dependency.junit-bom.version>
        <!--#endregion-->

        <!--#region plugins official clean lifecycle-->
        <plugin.clean.version>3.2.0</plugin.clean.version>
        <!--#endregion-->

        <!--#region plugins official default lifecycle-->
        <plugin.resources.version>3.2.0</plugin.resources.version>
        <plugin.compiler.version>3.10.1</plugin.compiler.version>
        <plugin.install.version>3.0.0-M1</plugin.install.version>
        <plugin.deploy.version>3.0.0-M2</plugin.deploy.version>
        <plugin.jar.version>3.2.2</plugin.jar.version>
        <plugin.surefire.version>3.0.0-M6</plugin.surefire.version>
        <!--#endregion-->

        <!--#region plugins official site lifecycle-->
        <plugin.site.version>3.7.1</plugin.site.version>
        <plugin.javadoc.version>3.4.0</plugin.javadoc.version>
        <plugin.project-info-reports.version>3.0.0</plugin.project-info-reports.version>
        <plugin.surefire-report.version>3.0.0-M6</plugin.surefire-report.version>
        <plugin.jxr.version>3.2.0</plugin.jxr.version>
        <!--#endregion-->

        <!--#region plugins coverage-->
        <plugin.jacoco.version>0.8.3</plugin.jacoco.version>
        <plugin.cobertura.version>2.7</plugin.cobertura.version>
        <!--#endregion-->

        <!--#region plugins static analysis-->
        <plugin.checkstyle.version>3.1.2</plugin.checkstyle.version>
        <plugin.checkstyle.dependency.version>10.3</plugin.checkstyle.dependency.version>
        <plugin.pmd.version>3.16.0</plugin.pmd.version>
        <plugin.pmd.p3c.version>2.1.1</plugin.pmd.p3c.version>
        <plugin.spotbugs.version>4.6.0.0</plugin.spotbugs.version>
        <!--#endregion-->

        <!--#region test phase-->
        <test-report.basedir>target/test-reports</test-report.basedir>
        <test-report.dynamic.basedir>${test-report.basedir}/dynamic</test-report.dynamic.basedir>
        <test-report.static.basedir>${test-report.basedir}/static</test-report.static.basedir>
        <plugin.jacoco.report.output.dir>${test-report.dynamic.basedir}/jacoco</plugin.jacoco.report.output.dir>
        <plugin.surefire-report.output.dir>${test-report.dynamic.basedir}/surefire
        </plugin.surefire-report.output.dir>
        <plugin.cobertura.report.dir>${test-report.dynamic.basedir}/cobertura</plugin.cobertura.report.dir>
        <plugin.checkstyle.output.dir>${test-report.static.basedir}/checkstyle</plugin.checkstyle.output.dir>
        <plugin.pmd.output.dir>${test-report.static.basedir}/pmd</plugin.pmd.output.dir>
        <!--#endregion-->
        
        <!--#region sonarcloud-->
        <sonar.organization>arm-courses</sonar.organization>
        <!--#endregion-->
    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- https://mvnrepository.com/artifact/org.junit/junit-bom -->
            <dependency>
                <groupId>org.junit</groupId>
                <artifactId>junit-bom</artifactId>
                <version>5.8.2</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement><!-- lock down plugins versions to avoid using Maven defaults (may be moved to parent pom) -->
            <plugins>
                <!--#region clean lifecycle, see https://maven.apache.org/ref/current/maven-core/lifecycles.html#clean_Lifecycle -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-clean-plugin</artifactId>
                    <version>${plugin.clean.version}</version>
                </plugin>
                <!--#endregion-->
                
                <!--#region default lifecycle, jar packaging: see https://maven.apache.org/ref/current/maven-core/default-bindings.html#Plugin_bindings_for_jar_packaging -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>${plugin.resources.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${plugin.compiler.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${plugin.surefire.version}</version>
                    <configuration>
                        <testFailureIgnore>true</testFailureIgnore>
                        <statelessTestsetReporter
                            implementation="org.apache.maven.plugin.surefire.extensions.junit5.JUnit5Xml30StatelessReporter">
                            <disable>false</disable>
                            <version>3.0</version>
                            <usePhrasedFileName>false</usePhrasedFileName>
                            <usePhrasedTestSuiteClassName>true</usePhrasedTestSuiteClassName>
                            <usePhrasedTestCaseClassName>true</usePhrasedTestCaseClassName>
                            <usePhrasedTestCaseMethodName>true</usePhrasedTestCaseMethodName>
                        </statelessTestsetReporter>
                        <consoleOutputReporter
                            implementation="org.apache.maven.plugin.surefire.extensions.junit5.JUnit5ConsoleOutputReporter">
                            <disable>false</disable>
                            <encoding>UTF-8</encoding>
                            <usePhrasedFileName>false</usePhrasedFileName>
                        </consoleOutputReporter>
                        <statelessTestsetInfoReporter
                            implementation="org.apache.maven.plugin.surefire.extensions.junit5.JUnit5StatelessTestsetInfoReporter">
                            <disable>false</disable>
                            <usePhrasedFileName>false</usePhrasedFileName>
                            <usePhrasedClassNameInRunning>true</usePhrasedClassNameInRunning>
                            <usePhrasedClassNameInTestCaseSummary>true</usePhrasedClassNameInTestCaseSummary>
                        </statelessTestsetInfoReporter>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>${plugin.jar.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-install-plugin</artifactId>
                    <version>${plugin.install.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>${plugin.deploy.version}</version>
                </plugin>
                <!--#endregion-->
                
                <!--#region site lifecycle, see https://maven.apache.org/ref/current/maven-core/lifecycles.html#site_Lifecycle -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-site-plugin</artifactId>
                    <version>${plugin.site.version}</version>
                </plugin>

                <!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-javadoc-plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>${plugin.javadoc.version}</version>
                </plugin>
                <!--#endregion-->

                <!-- self-defined phase in default lifecycle -->
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${plugin.jacoco.version}</version>
                    <executions>
                        <execution>
                            <id>init-jacoco</id>
                            <!-- default bound to phase: initialize -->
                            <goals>
                                <goal>prepare-agent</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>jacoco-reports</id>
                            <!-- default bound to phase: verify -->
                            <goals>
                                <goal>report</goal>
                            </goals>
                            <configuration>
                                <outputDirectory>${plugin.jacoco.report.output.dir}</outputDirectory>
                            </configuration>
                        </execution>
                        <execution>
                            <id>jacoco-check</id>
                            <phase>test</phase>
                            <!-- Bound to phase: verify-->
                            <goals>
                                <goal>check</goal>
                            </goals>
                            <configuration>
                                <rules>
                                    <rule>
                                        <element>PACKAGE</element>
                                        <limits>
                                            <limit>
                                                <counter>BRANCH</counter>
                                                <value>COVEREDRATIO</value>
                                                <minimum>0.80</minimum>
                                            </limit>
                                        </limits>
                                    </rule>
                                </rules>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>

                <!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-checkstyle-plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <version>${plugin.checkstyle.version}</version>
                    <executions>
                        <execution>
                            <id>static-check-checkstyle</id>
                            <phase>test</phase>
                            <goals>
                                <goal>checkstyle</goal>
                            </goals>
                            <configuration>
                                <outputDirectory>${plugin.checkstyle.output.dir}</outputDirectory>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>

                <!-- reporting plugin start -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-report-plugin</artifactId>
                    <version>${plugin.surefire-report.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>cobertura-maven-plugin</artifactId>
                    <version>${plugin.cobertura.version}</version>
                    <executions>
                        <execution>
                            <id>cobertura-report</id>
                            <goals>
                                <goal>cobertura</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-project-info-reports-plugin</artifactId>
                    <version>${plugin.project-info-reports.version}</version>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jxr-plugin</artifactId>
                    <version>${plugin.jxr.version}</version>
                </plugin>
                <plugin>
                    <groupId>com.github.spotbugs</groupId>
                    <artifactId>spotbugs-maven-plugin</artifactId>
                    <version>${plugin.spotbugs.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-pmd-plugin</artifactId>
                    <version>${plugin.pmd.version}</version>
                    <configuration>
                        <rulesets>
                            <!-- PMD ruleset -->
                            <ruleset>/category/java/bestpractices.xml</ruleset>
                            <ruleset>/category/java/codestyle.xml</ruleset>
                            <ruleset>/category/java/design.xml</ruleset>
                            <ruleset>/category/java/documentation.xml</ruleset>
                            <ruleset>/category/java/errorprone.xml</ruleset>
                            <ruleset>/category/java/multithreading.xml</ruleset>
                            <ruleset>/category/java/performance.xml</ruleset>
                            <!-- P3C ruleset -->
                            <ruleset>rulesets/java/ali-comment.xml</ruleset>
                            <ruleset>rulesets/java/ali-concurrent.xml</ruleset>
                            <ruleset>rulesets/java/ali-constant.xml</ruleset>
                            <ruleset>rulesets/java/ali-exception.xml</ruleset>
                            <ruleset>rulesets/java/ali-flowcontrol.xml</ruleset>
                            <ruleset>rulesets/java/ali-naming.xml</ruleset>
                            <ruleset>rulesets/java/ali-oop.xml</ruleset>
                            <ruleset>rulesets/java/ali-orm.xml</ruleset>
                            <ruleset>rulesets/java/ali-other.xml</ruleset>
                            <ruleset>rulesets/java/ali-set.xml</ruleset>
                        </rulesets>
                        <printFailingErrors>true</printFailingErrors>
                        <outputDirectory>${plugin.pmd.output.dir}</outputDirectory>
                    </configuration>
                    <executions>
                        <execution>
                            <id>static-check-pmd</id>
                            <phase>test</phase>
                            <goals>
                                <goal>pmd</goal>
                            </goals>
                        </execution>
                    </executions>
                    <dependencies>
                        <!-- https://mvnrepository.com/artifact/com.alibaba.p3c/p3c-pmd -->
                        <dependency>
                            <groupId>com.alibaba.p3c</groupId>
                            <artifactId>p3c-pmd</artifactId>
                            <version>${plugin.pmd.p3c.version}</version>
                        </dependency>
                    </dependencies>
                </plugin>
                <!-- reporting plugin end-->
            </plugins>
        </pluginManagement>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-report-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jxr-plugin</artifactId>
            </plugin>
        </plugins>
    </reporting>
</project>
