# Unit Testing Evolution with JUnit4

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Requirements of Test Script](#requirements-of-test-script)
2. [Iterations of the Test Script](#iterations-of-the-test-script)
3. [Bibliographies](#bibliographies)

<!-- /code_chunk_output -->

## Requirements of Test Script

### Basic Requirements

1. 自动运行：运行测试用例，得到`actual`
1. 自动校验：校验用例结果，比较`expected`与`actual`
1. 自动判断：判断测试结果，判断`pass`还是`failure`
1. 自动记录：记录测试结果，记录测试结果及其上下文（如果`failure` ==> 则记录`defect`及其上下文，用于定位`defect`）
1. 自动报告：报告测试结果，统计、分析、可视化测试结果集

### Advanced Requirements

1. 更简单的编程：更简单的结构、更少的代码量、更可复用的代码
1. 更现代的设计
    1. 数据与算法分离：数据驱动/数据参数化
    1. 全程自动化测试：事件驱动，测试全部
1. 更方便的管理：可管理、可扩展、持久化、工具链
1. 更高的复用率：一次编写，反复（回归）测试

## Iterations of the Test Script

### Based On Simulation

1. 基于`main()`的、模拟的、手工的测试脚本
   1. `debug`：人工输入、人工校验、人工判断、人工记录
       - [`BodyMassIndex`](src/main/java/org/arm/crse/sq/evolution/BodyMassIndex.java)
2. 基于`main()`的、模拟的、自动的测试脚本
   1. 独立的测试脚本：自动输入、自动校验、自动判断、自动记录
       - [`TestScheme01`](src/main/java/org/arm/crse/sq/evolution/test/TestScheme01.java)
       - [`TestScheme02`](src/main/java/org/arm/crse/sq/evolution/test/TestScheme02.java)
   2. 分离的测试脚本：测试代码与生产代码相分离、测试用例独立测试、抽取并封装相同代码
       - [`TestScheme03`](src/test/java/org/arm/crse/sq/evolution/test/TestScheme03.java)

### Based On Testing Framework

基于设计的测试脚本

1. 基于测试框架
    - [`step01/basis/BodyMassIndexTest`](src/test/java/org/arm/crse/sq/evolution/step01/basis/BodyMassIndexTest.java)
    - [`step01/reuse/BodyMassIndexTest`](src/test/java/org/arm/crse/sq/evolution/step01/reuse/BodyMassIndexTest.java)
2. 测试用例独立测试
    - [`step02/independent/BodyMassIndexTest`](src/test/java/org/arm/crse/sq/evolution/step02/independent/BodyMassIndexTest.java)
3. 参数化测试
    1. 手动注入`ManualInjection`
        - [`step03/parameterized/mi/BodyMassIndexTest`](src/test/java/org/arm/crse/sq/evolution/step03/parameterized/mi/BodyMassIndexTest.java)
    1. 构造方法注入`ConstructorInjection`
        - [`step03/parameterized/ci/BodyMassIndexTest`](src/test/java/org/arm/crse/sq/evolution/step03/parameterized/ci/BodyMassIndexTest.java)
    1. 属性注入`PropertyInjection`
        - [`step03/parameterized/pi/BodyMassIndexTest`](src/test/java/org/arm/crse/sq/evolution/step03/parameterized/pi/BodyMassIndexTest.java)
4. 分类测试
    - [`step04/categorized/BodyMassIndexTest`](src/test/java/org/arm/crse/sq/evolution/step04/categorized/BodyMassIndexTest.java)
5. 测试集
    - [`step05/suite/TestSuite`](src/test/java/org/arm/crse/sq/evolution/step05/suite/TestSuite.java)
6. 分类测试集
    - [`step06/suite/categorized/TestSuite`](src/test/java/org/arm/crse/sq/evolution/step06/suite/categorized/TestSuite.java)
7. 参数化分类测试集
   1. [`step07/parameterized/categorized/TestSuite`](src/test/java/org/arm/crse/sq/evolution/step07/parameterized/categorized/TestSuite.java)
8. 自动报告：持久化测试结果，形成测试报告
    - maven plugin [`maven-surefire-report-plugin`](https://maven.apache.org/surefire/maven-surefire-report-plugin/)

## Run the Testing Script

### IDE Mode

点击运行相应的测试方法、测试类或测试集

### Maven Mode

#### MacOS and Linux

```shell
./mvnw test
./mvnw site
```

#### Windows

```shell
mvnw.cmd test
mvnw.cmd site
```

## Bibliographies

1. <https://www.icourse163.org/learn/HUST-1001907003?tid=1450680457&from=study#/learn/content?type=detail&id=1215558879>
