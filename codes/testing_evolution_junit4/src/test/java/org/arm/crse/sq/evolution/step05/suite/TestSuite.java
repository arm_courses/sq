package org.arm.crse.sq.evolution.step05.suite;

import org.arm.crse.sq.evolution.step03.parameterized.ci.BloodPressureTest;
import org.arm.crse.sq.evolution.step03.parameterized.ci.BodyMassIndexTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({BloodPressureTest.class, BodyMassIndexTest.class})
public class TestSuite {
}
