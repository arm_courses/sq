package org.arm.crse.sq.evolution.step01.basis;

import org.arm.crse.sq.evolution.BodyMassIndex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BodyMassIndexTest {

    private BodyMassIndex testObj;

    @Test
    public void testGetBMIType_Thin(){
        testObj = new BodyMassIndex(45.0,1.6);
        String expected = "偏瘦";
        assertEquals(expected,testObj.getLevel());
    }

    @Test
    public void testGetBMIType_Normal(){
        testObj = new BodyMassIndex(55.0,1.6);
        String expected = "正常";
        assertEquals(expected,testObj.getLevel());
    }

    @Test
    public void testGetBMIType_SlightlyFat() {
        testObj = new BodyMassIndex(68.0,1.6);
        String expected = "偏胖";
        assertEquals(expected,testObj.getLevel());
    }

    @Test
    public void testGetBMIType_Fat() {
        testObj = new BodyMassIndex(80.0, 1.6);
        String expected = "肥胖";
        assertEquals(expected,testObj.getLevel());
    }

    @Test
    public void getBMIType() {
    }
}