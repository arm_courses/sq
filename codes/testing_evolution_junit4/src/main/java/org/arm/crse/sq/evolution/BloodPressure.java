package org.arm.crse.sq.evolution;

/**
 * @author aRoming 
 */
public class BloodPressure {

    public static final String NORMAL = "正常";
    public static final String PRE_HYPERTENSION = "正常高值";
    public static final String HYPERTENSION_LEVEL_ONE = "1级高血压";
    public static final String HYPERTENSION_LEVEL_TWO = "2级高血压";
    public static final String HYPERTENSION_LEVEL_THREE = "3级高血压";
    public static final String ABNORMAL_VALUE = "异常值";
    /**
     * 收缩压
     */
    private int systolicPressure;
    /**
     * 舒张压
     */
    private int diastolicPressure;

    public BloodPressure() {
        this.init(0, 0);
    }

    public BloodPressure(int systolicPressure, int diastolicPressure) {
        this.init(systolicPressure, diastolicPressure);
    }

    private void init(int systolicPressure, int diastolicPressure) {
        this.systolicPressure = systolicPressure;
        this.diastolicPressure = diastolicPressure;
    }

    public void setValues(int systolicPressure, int diastolicPressure) {
        this.init(systolicPressure, diastolicPressure);
    }

    /**
     * 根据血压值判断所属血压分级
     *
     * @return String: blood pressure level
     */
    public String getLevel() {
        String level = null;
        if (systolicPressure < 120 && diastolicPressure < 80) {
            level = NORMAL;
        } else if ((systolicPressure >= 120 && systolicPressure <= 139) &&
            (diastolicPressure >= 80 && diastolicPressure <= 89)) {
            level = PRE_HYPERTENSION;
        } else if ((systolicPressure >= 140 && systolicPressure <= 159) ||
            (diastolicPressure >= 90 && diastolicPressure <= 99)) {
            level = HYPERTENSION_LEVEL_ONE;
        } else if ((systolicPressure >= 160 && systolicPressure <= 179) ||
            (diastolicPressure >= 100 && diastolicPressure <= 109)) {
            level = HYPERTENSION_LEVEL_TWO;
        } else if ((systolicPressure >= 180) || (diastolicPressure >= 110)) {
            level = HYPERTENSION_LEVEL_THREE;
        } else {
            level = ABNORMAL_VALUE;
        }
        return level;
    }

}
