package org.arm.crse.sq.evolution;

import java.util.Scanner;

/**
 * @author aRoming
 */
public class BodyMassIndex {
    public static final String OBESITY = "肥胖";
    public static final String OVER_WEIGHT = "偏胖";
    public static final String NORMAL_WIGHT = "正常";
    public static final String UNDER_WEIGHT = "偏瘦";
    private double weight;
    private double height;

    public BodyMassIndex() {
        setValues(0.0, 0.0);
    }

    public BodyMassIndex(final double weight, final double height) {
        this.init(weight, height);
    }

    /**
     * debug test main method
     *
     * @param args no need args
     */
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        String prompt = "请输入体重和身高，以等号=结束";
        double w = 0.0;
        double h = 0.0;
        System.out.println(prompt);
        while (reader.hasNextDouble()) {
            w = reader.nextDouble();
            h = reader.nextDouble();
        }
        BodyMassIndex testObj = new BodyMassIndex(w, h);
        String result = testObj.getLevel();
        String output = "体重：" + w + "，身高：" + h + "，BMI状况是：" + result;
        System.out.print(output);
    }

    private void init(final double weight, final double height) {
        if (weight < 0 || height < 0) {
            throw new IllegalArgumentException("weight and height must greater or equal to 0...");
        }
        this.weight = weight;
        this.height = height;
    }

    public void setValues(final double weight, final double height) {
        this.init(weight, height);
    }
    
    /**
     * BMI Categories(domestic):
     * Underweight: [0, 18.5 )
     * Normal weight: [18.5 – 24)
     * Overweight: [24 – 28)
     * Obesity: 28 or greater
     *
     * @return String represent the category of BMI
     */
    public String getLevel() {
        double bmi = 0.0;

        //TODO: should init with empty string
        String ret = "";

        //TODO: how to improve the if-else
        if (weight > 0 && height > 0) {
            bmi = weight / (height * height);

            if (bmi < 18.5) {
                ret = UNDER_WEIGHT;
            } else if (bmi < 24) {
                ret = NORMAL_WIGHT;
            } else if (bmi < 28) {
                ret = OVER_WEIGHT;
            } else {
                ret = OBESITY;
            }
        } else {
            throw new IllegalStateException("weight or height error...");
        }
        return ret;
    }
}
