package org.arm.crse.sq;


/**
 * @author aRoming
 */
public class SampleLogic {

    public int process(int x, int y) {
        //s1
        int ret = 0;
        if (x == 0 && y > 2) {
            //s2
            ret += 1;
        } 
        if (x < 1 || y == 1) {
            //s3
            ret += 2;
        }
        //s4
        ret += x + y;
        return ret;
    }
}
