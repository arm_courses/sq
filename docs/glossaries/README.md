# 相关术语Terminology

## 参考文献Bibliographies

1. **_`C-SWEBOK-2018`_**. 教育部高等学校软件工程专业教学指导委员会C-SWEBOK编写组. 中国软件工程知识体系C-SWEBOK[M]. 北京:高等教育出版社.2018-03.
1. **_`GB/T 11457-2006`_**.中华人民共和国国家质量监督检验检疫总局, 中国国家标准化管理委员会. GB/T 11457-2006 信息技术 软件工程术语[S].北京:中国标准出版社. 2006-03-14（发布）, 2006-07-01（实施）.
1. **_`GB/T 19000-2016/ISO 9000:2015`_**. 中华人民共和国国家质量监督检验检疫总局, 中国国家标准化管理委员会. GB/T 19000-2016/ISO 9000:2015质量管理体系 基础和术语[S].北京:中国标准出版社. 2016-12-30（发布）, 2017-07-01（实施）.
1. **_`GB/T 19003-2008/ISO/IEC 90003:2004`_**.中华人民共和国国家质量监督检验检疫总局, 中国国家标准化管理委员会. GB/T 19001-2000应用于计算机软件的指南[S].北京:中国标准出版社.2008-06-18（发布）,2008-11-01（实施）.
1. **_`Wikipedia`_**. Wikimedia Foundation, Inc. . Wikipedia[OL/DB]. <https://www.wikipedia.org/>
1. **_`中文维基百科`_**. 维基媒体基金会. 维基百科（中文）. <https://zh.wikipedia.org/>
1. **_`IEEE 610.12:1990`_**. IEEE Standards Board. IEEE Std 610.12-1990 IEEE Standard Glossary of Software Engineering Terminology[S]. 1990-09-28. ISBN: 1-55937-067-X
1. **_`ISO/IEC/IEEE 24765:2010(E)`_**. ISO, IEC, IEEE. Systems and software engineering - Vocabulary[S]. 2010-12-15. 
