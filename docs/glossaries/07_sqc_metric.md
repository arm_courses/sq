# 测试度量

1. 逻辑覆盖率:Logical Coverage
    1. 代码覆盖率:Code coverage
    1. 结构化覆盖率:Structural Coverage
1. 语句覆盖率:Statement Coverage
1. 判定覆盖:Decision Coverage
    1. 分支覆盖:Branch Coverage
1. 条件覆盖:Condition Coverage
1. 判定条件覆盖:Decision Condition Coverage
    1. 分值条件:Branch Condition Coverage,BC Coverage
1. 路径覆盖:Path Coverage
1. 指令覆盖:Instruction Blocks Coverage, IB Coverage
1. 判定路径覆盖:Decision-to-Decision Paths Coverage,DDP Coverage
1. 功能覆盖率:Function Coverage
