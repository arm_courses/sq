# Slides

```bash {.line-numbers cmd=true output=text hide=false run_on_save=true}
tree -I *_code_chunk -I *.html -I *.pdf -d .
```

```bash {.line-numbers}
.
├── p01_sq_and_sqm
│   ├── c01_sq_intro
│   └── c02_sqm_standards
├── p02_sqc
│   ├── c01_testing_intro
│   ├── c02_black-box_testing
│   ├── c03_white-box_testing
│   ├── c04_testing_phases
│   │   ├── s01_low-level_testing
│   │   └── s02_high-level_testing
│   ├── c05_static_testing_and_code_review
│   └── c06_testing_management
└── p03_sqa
    ├── c01_sqa_intro
    ├── c02_scm
    └── c03_software_review
```
