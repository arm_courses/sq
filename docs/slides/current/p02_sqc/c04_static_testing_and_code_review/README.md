---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Static Testing and Code Review_"
footer: "@aRoming"
math: katex
---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# 静态测试与代码审查Static Testing and Code Review

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [静态测试](#静态测试)
2. [🌟 代码风格Coding Style](#代码风格coding-style)
3. [代码评审Code Review](#代码评审code-review)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 静态测试

---

1. 静态测试：在不运行被测软件的情况下，通过分析或检查软件（源代码或目标代码或文档或数据）以验证软件的正确性、找出其中的缺陷
1. 自动静态测试：源代码分析、目标代码分析等
1. 人工静态测试：评审（`ad hoc review`, `pass around`, `walk through`, `review`, `inspection`）

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 代码风格Coding Style

>1. [Programming Style.](https://en.wikipedia.org/wiki/Programming_style)
>1. [Coding Conventions.](https://en.wikipedia.org/wiki/Coding_conventions)

---

### 代码风格是什么

1. `代码风格`也称为`编码约定`、`编程风格`
1. `programming style`, `code style`, `coding Style`, `coding conventions`

---

>`Programming style`, also known as `code style`, is _**a set of rules or guidelines**_ used when writing the source code for a computer program. It is often claimed that following a particular programming style will _**help programmers read and understand source code**_ conforming to the style, and _**help to avoid introducing errors**_.

---

### 代码风格的必要性

1. 增强代码健壮性
1. 减少协作成本：可读性、可维护性、可移植性
1. 提升项目资源复用率

👉 统一的、良好的代码风格，是一个优秀而且职业化的开发团队所必需的素质 👈

---

### 代码风格的内容

1. 工程目录结构：源文件间的组织结构
1. 代码结构：源文件内的语句规范、排版规范等
1. 注释规范/文档规范
1. 命名规范
1. 接口规范
1. 其他与编码相关的内容

>"Style" covers a lot of ground, from "use camelCase for variable names" to "never use global variables" to "never use exceptions."

---

### 典型代码风格 -- Google Style Guides

1. [Google. Google Style Guides.](https://google.github.io/styleguide/)
1. [Google 开源项目风格指南——中文版.](http://zh-google-styleguide.readthedocs.org/)

---

### 典型代码风格 -- Linux Kernel Coding Style

1. [Linux kernel coding style.](https://www.kernel.org/doc/html/latest/process/coding-style.html)
1. [Linux 内核代码风格.](https://www.kernel.org/doc/html/latest/translations/zh_CN/process/coding-style.html)

---

### 典型代码风格 -- Alibaba Java Coding Guidelines

1. [Alibaba Java开发手册.](https://developer.aliyun.com/special/tech-java?spm=a2c41.13037006.0.0)
1. [GitHub. Alibaba/P3C.](https://github.com/alibaba/p3c)

---

### 命名规范Naming Convention

>1. <https://en.wikipedia.org/wiki/Naming_convention_(programming)>
>1. <https://zh.wikipedia.org/wiki/%E5%91%BD%E5%90%8D%E8%A7%84%E5%88%99_(%E7%A8%8B%E5%BA%8F%E8%AE%BE%E8%AE%A1)>

---

#### 典型命名规范 – 匈牙利命名法Hungarian Notation

1. 据传由`MS`的匈牙利程序员 _Charles Simonyi_ 发明，在`Windows API`和`VC++`中得到广泛应用
1. 基本原则：`标识符名称 = 属性 + 数据类型 + 对象描述`，`属性`和`数据类型`使用小写字母，`对象描述`使用首字母大写

>1. <https://en.wikipedia.org/wiki/Hungarian_notation>
>1. <https://zh.wikipedia.org/wiki/%E5%8C%88%E7%89%99%E5%88%A9%E5%91%BD%E5%90%8D%E6%B3%95>

---

#### 典型命名法规范 – 驼峰命名法Camel Case

1. `upper camel case大驼峰命名法`： 又称`Pascal Case帕斯卡命名法`，所有单词首字母大写
1. `lower camel case小驼峰命名法`： 首单词首字母小写，剩余单词首字母大写

>1. <https://en.wikipedia.org/wiki/Camel_case>
>1. <https://zh.wikipedia.org/wiki/%E9%A7%9D%E5%B3%B0%E5%BC%8F%E5%A4%A7%E5%B0%8F%E5%AF%AB>

---

#### 典型命名规范 – 下划线命名法Snake Case

1. 单词间用下划线连接
1. 变量单词均使用小写
1. 常量单词均使用大写

>1. <https://en.wikipedia.org/wiki/Snake_case>

---

#### 命名规则 – 单词缩写规则

1. 较短的单词可通过去掉"元音"形成缩写
    - temp ==> tmp
    - flag ==> flg
1. 较长的单词可取单词的头几个字母形成缩写
    - configuration ==> config

---

#### 命名规则 – 常用的公认的单词缩写

1. configuration ==> config/cfg
1. temp ==> tmp
1. flag ==> flg
1. statistic ==> stat
1. increment ==> inc
1. message ==> msg

><https://www.allacronyms.com>

---

1. 🌟 命名规范尽量与所采用的`OS`或开发平台的风格保持一致
1. 应该在源文件的开始之处，对文件中所使用的缩写或约定， **_特别是特殊的或自定义的缩写_**，进行必要的注释说明
1. 标识符最好采用英文单词或其组合，避免使用汉语拼音进行命名，避免仅以数字编号进行区分（如：`value_1`, `value_2`）

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 代码评审Code Review

---

### 代码评审是什么What is Code Review

>1. `code review` (sometimes referred to as `peer review`) is a `software quality assurance` activity in which _**one or several people**_ check a program mainly by _**viewing and reading parts of its source code**_, and they do so after implementation or as an interruption of implementation.
>1. _**at least**_ one of the persons must not be the code's author.
>1. the persons performing the checking, excluding the author, are called "reviewers".
>
>[wiki.code review.](https://en.wikipedia.org/wiki/Code_review)

---

>同行评审（peer review，在某些学术领域亦称refereeing），或译为同行评议、同侪评阅、同侪审查，是一种学术成果审查程序，即一位作者的学术著作或计划被同一领域的其他专家学者评审。同行评审程序的主要目的是：确保作者的著作水平符合一般学术与该学科领域的标准。
>
>一般学术出版单位，主要以同行评审的方法来选择与筛选所投送的稿件录取与否；而学术研究资金提供机构，也广泛以同行评审的方式来决定研究是否授予资金、奖金等。在许多领域，著作的出版或者研究奖金的颁发，如果没有以同行评审的方式来进行就可能比较会遭人质疑，甚至成为某出版物、作品是否可以被称为学术出版物的主要标准。
>
>[wiki.同行评审](https://zh.wikipedia.org/wiki/同行評審)

---

### 代码评审的主要内容

1. 发现`bug`：人工检查以发现代码中的`bug`
1. 检查易读性：在自动化静态测试的基础上进一步检查是否符合风格规范，检查代码是否清晰易懂
1. 检查易维护性、可扩展性

👉 `code review`作为人工卡点，检查自动化测试难于完成的工作，进而最终确认代码是否通过 👈

---

### 代码评审的责任人

1. 代码作者承担主要责任
1. 代码评审员承担次要责任

---

### 代码评审的流程（传统方式）

1. 代码作者对照`SRS`讲解代码逻辑
1. 代码评审员可随时提出疑问，并记录在案
1. 代码作者讲解完后，代码评审员进行代码审查
1. 代码评审员根据审查结果编写`"代码审查报告"`并发送给相关人员
1. 代码作者根据`"代码审查报告"`修改代码
1. 代码作者反馈`bugfix`
1. 重回初始步骤，直至不再发现`bug`

---

### 代码评审的流程（基于`Git`方式）

![height:600](./.assets/image/pull-based-work-flow-on-github.jpg)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
