---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Software Quality Standards_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 软件质量标准Software Quality Standards

---

## Contents

1. :star2: 标准的类型/层次
1. :star2: 质量管理体系`ISO 9000`、`ISO 9001`与`ISO 9000-3`
1. :star2: 成熟度模型`CMM`与`CMMI`
1. `IEEE`软件工程标准
1. 其他标准

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 标准的类型/层次

>1. <https://www.jianshu.com/p/8c0cdcb21789>

---

1. `standard标准`
1. `specification规范`
1. `convention规约，惯例`
1. `style风格`

---

+ 标准的类型/层次
    1. 国际标准： 由国际机构颁布的，供各国 **_参考_** 的标准
    1. 国家标准： 由国家机构颁布的，适用于本国的标准
    1. 行业标准： 由行业机构、学术团体或国防机构颁布的，适用于某个业务领域的标准
    1. 企业规范： 由企业颁布的，适用于本企业的规范
    1. 项目规范： 由项目组织颁布的，适用于本项目的规范
+ 项目规范一般为具体的操作规范，主要特点有：目标明确、项目专用、使用范围小

---

### 国际机构/国际标准

1. :star2: 国际标准化组织（International Standards Organization, `ISO`）
    1. Founded: February 23, 1947, London, United Kingdom
    1. Headquarters: Geneva, Switzerland
1. :star2: 国际电工委员会（International Electrotechnical Commission, `IEC`）
    1. Founded: June 26, 1906, London, United Kingdom
    1. Headquarters: Geneva, Switzerland
1. :star2: 国际电信联盟（International Telecommunication Union, `ITU`）
    1. Founded: May 17, 1865
    1. Headquarters: Geneva, Switzerland

---

1. :star2: 欧洲计算机制造联合会（European Computer Manufactures Association, `ECMA`）
    1. Founded: 1961
    1. Headquarters: Geneva, Switzerland
1. 欧洲电信标准协会（European Telecommunications Standards Institute, `ETSI`）
    1. Founded: 1988
    1. Headquarters: Sophia Antipolis, France

---

1. :star2: 万维网联盟（World Wide Web Consortium, `W3C`）
    1. Founded: October 1, 1994
    1. Headquarters: Cambridge, MA
1. Linux基金会（The Linux Foundation, `LF`）
    1. Founded: 2000
    1. Headquarters: San Francisco, CA

---

### 国家机构/国家标准

1. :star2: 中华人民共和国国家标准（GuoBiao, `GB`）：`GB`、`GB/T`、`GB/Z`
1. :star2: 美国国家标准协会（American National Standards Institute, `ANSI`）
1. 美国国家标准技术学会（National Institute of Standards and Technology, `NIST`）
1. 美国联邦信息处理标准（Federal Information Processing Standards, `FIPS`）：`NIST`针对联邦政府计算机系统制订的标准和准则

---

1. 英国国家标准协会（British Standard Institute, `BSI`）
1. 德国国家标准协会（Deutsches Institut für Normung, `DIN`）
1. 日本工业标准调查会（Japanese Industrial Standards Committee, `JISC`）
1. 日本工业标准（Japanese Industrial Standard, `JIS`）：由`JISC`制定的日本国家级标准

---

### 行业组织/行业标准

1. :star2: 美国电气和电子工程师学会（Institute Of Electrical and Electronics Engineers, `IEEE`）
1. 美国能源信息署（U.S. Energy Information Administration, `EIA`）
1. 美国国防部标准（Department of Defense-Standards, `DOD-STD`）
1. 中华人民共和国国家军用标准（GuoJunBiao, `GJB`）
1. 美国军用标准（Military-Standards, `MIL-S`）

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 质量管理体系ISO 9000 Family

---

### ISO9000质量管理体系The ISO 9000 family of QMS(Quality Management Systems)

1. `ISO 9000:2015`: Quality Management Systems - Fundamentals and Vocabulary (definitions)
1. `ISO 9001:2015`: Quality Management Systems - Requirements
1. `ISO 9004:2018`: Quality Management - Quality of an Organization - Guidance to Achieve Sustained Success (continuous improvement)
1. `ISO 19011:2018`: Guidelines for Auditing Management Systems

>[What is The ISO 9000 Standards Series?](https://asq.org/quality-resources/iso-9000)

---

1. `ISO 9001:1987`: Model for quality assurance in design, development, production, installation, and servicing was for companies and organizations whose activities included the creation of new products.
1. `ISO 9002:1987`: Model for quality assurance in production, installation, and servicing had basically the same material as ISO 9001 but **_without covering the creation of new products_**.
1. `ISO 9003:1987`: Model for quality assurance in final inspection and test covered only the final inspection of finished product, **_with no concern for how the product was produced_**.

>[ISO 9000](https://en.wikipedia.org/wiki/ISO_9000#2015_version)

---

### Industry-Specific Interpretations

1. `ISO/IEC 90003:2014`: provides guidelines for the application of ISO 9001 to computer software.
1. `TickIT`: The TickIT guidelines are an interpretation of ISO 9000 produced by the UK Board of Trade to suit the processes of the information technology industry, especially software development.

>[ISO 9000](https://en.wikipedia.org/wiki/ISO_9000#2015_version)

---

+ `ISO 9001`：设计、开发、生产、安装、服务的`QM`模型
    1. 属于`ISO 9000`簇标准体系，适用于所有工程行业
    1. 通过认证所必须实现的20条需求，针对每一条需求建立相应的政策和过程，并且依照这些政策和过程开展生产活动
+ 通过`ISO 9000`认证，指的是通过`ISO 9001`认证（`ISO 9000`是基础与词汇表、`ISO 9004`无需认证、`ISO 19011`是指南、`ISO 9002`和`ISO 9003`已不再使用）
+ `ISO/IEC 90003`（替代`ISO 9000-3`）： 基于软件行业的特殊性制定的`ISO 9001`的 **_使用指南_**，不作为`ISO 9001`注册/认证时的评估准则

>[ISO 9002 & ISO 9003 are History](https://advisera.com/9001academy/knowledgebase/iso-9002-iso-9003-are-history/)

---

### 现行GB/T 19000与ISO 9000对应（采用关系）

---

1. `GB/T 19000-2018` <==> `ISO 9000:2015`
1. `GB/T 19001-2016` <==> `ISO 9001:2015`
    1. `GB/T 19002-2018`：`GB/T 19001-2016`应用指南
    1. `GB/T 18305-2016`：汽车生产件及相关服务件组织应用`GB/T 19001-2016`特别要求
    1. `GB/T 27865-2011`：危险货物包装 包装、中型散装容器、大包装`GB/T 19001`的应用指南
    1. `GB/T 19003-2008`：软件工程`GB/T 19001-2000`应用于计算机软件的指南
    1. `GB/T 19080-2003`：食品与饮料行业`GB/T 19001-2000`应用指南

---

1. `GB/T 19003-2008` <==> `ISO/IEC 90003:2004`
1. `GB/T 19004-2011` <==> `ISO 9004-2009`
    + `GB/T 19004-2020` <==> `ISO 9004:2018`(2021/06/01起实施)
1. `GB/T 19011-2013` <==> `ISO 19011:2011`

---

### TickIT项目TickIT Project

+ 1980s末，软件开发过程的特殊性使软件企业在应用`ISO 9001`标准时陷入了困境
+ 软件企业需要在通用认证过程的基础上补充附加的要求，导致了`TickIT`认证项目的产生
+ `TickIT`项目帮助软件企业建立与其业务过程相关的质量体系，并使该体系满足`ISO 9001`的要求

---

+ `TickIT`项目要求企业的第三方质量管理体系认证应由经认可的认证机构实施，而认证审核活动应通过使用那些在软件行业及其过程方面有直接经验的审核员完成
+ `TickIT`项目由`TickIT`办公室进行管理，它是`BSI(Britain Standard Institute, 英国标准学会)`专门负责所有信息系统和通信标准化工作的部门
+ `IRCA TickIT`审核员项目通过向第三方认证提供`TickIT`审核员及审核员培训课程来支持`TickIT`认证项目的实施。
+ 审核员IT能力要求基本指南由英国计算机协会的`TickIT`委员会制定

---

![height:600](./assets_image/Tick_IT.png)

---

### `ISO 9001`的20条需求

---

1. 管理职责
1. 质量系统
1. 合同复审
1. 设计控制
1. 文档和数据控制
1. 对客户提供产品控制
1. 产品标识和可跟踪性
1. 过程控制
1. 审查和测试
1. 审查、度量和测试设备的控制

---

1. 审查和测试状态
1. 对不符合标准的产品的控制
1. 改正和预防行为
1. 处理、存储、包装、保存、交付
1. 质量记录的控制
1. 内部质量审计
1. 培训
1. 服务
1. 统计技术
1. 采购

---

### `ISO 9000-3`核心内容

1. 合同评审
1. 需求规格说明
1. 开发计划
1. 质量计划
1. 设计和实现
1. 测试和确认
1. 验收
1. 复制、交付和安装
1. 维护

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `CMM(Capability Maturity Model, 能力成熟度模型)`与`CMMI(Capacity Maturity Model Integrated, 能力成熟度集成模型)`

---

+ `CMM`关注软件管理工程
+ `SW-CMM(Capability Maturity Model for Software, 软件能力成熟度模型)`
+ `CMM`是对软件组织在定义、实现、度量，控制和改善其软件过程的进程中各个发展阶段的描述
+ 通过5个不断进化的层次来评定软件生产的历史与现状，即5等级、18过程域、52目标、300多关践实践
+ 关系到软件项目成功与否的众多因素中，软件度量、工作量估计、项目规划、进展控制、需求变化、风险管理等，都是与工程管理直接相关的因素

---

![height:500](./assets_image/cmm_levels_01.png)

---

![height:600](./assets_image/cmm_levels_02.png)

---

|等级|特征|主要解决问题|关键域|结果|
|--|--|--|--|--|
|V优化级|软件过程的 **_量化反馈_** 和 **_新的思想和技术_** 促进过程的 **_不断改进_**|保持优化的机构|缺陷预防，过程变更和技术变更管理||
|IV已管理级|收集软件过程、产品质量的详细度量，对软件过程和产品质量有定量的理解和控制|技术变更、问题分析、问题预防|定量的软件过程管理、产品质量管理||

---

|等级|特征|主要解决问题|关键域|结果|
|--|--|--|--|--|
|III已定义级|已经将软件管理和过程文档化，标准化，同时综合成该组织的标准软件过程，所有的软件开发都使用该标准软件过程|过程度量、过程分析量化质量计划|组织过程定义，组织过程焦点，培训大纲，软件集成管理，软件产品工程，组织协调，专家评审|生产率和质量|

---

|等级|特征|主要解决问题|关键域|结果|
|--|--|--|--|--|
|II可重复级|建立了基本的项目管理来跟踪进度，费用和功能特征，制定了必要的项目管理，能够利用以前类似项目应用取得成功|培训、测试、技术常规和评审过程关注、标准和过程|需求管理，项目计划，项目跟踪和监控，软件子合同管理，软件配置管理，软件质量保证|风险|

---

|等级|特征|主要解决问题|关键域|结果|
|--|--|--|--|--|
|I初始级|软件过程混乱无序，对过程几乎没有定义，成功依靠个人的才能和经验，反应式管理方式|项目管理、项目策划、配置管理软件质量保证||

---

![height:600](./assets_image/cmm_levels_03.png)

---

### `KPA(Key Process Area, 关键过程域/关键域)`

---

![height:600](./assets_image/cmm_contents.png)

---

### `CMM1初始级Initial`

+ process is unpredictable, poorly controlled and reactive
+ 初始级处于`CMM`的最低级
+ 基本上没有健全的软件工程管理制度
+ 每件事情都以特殊的方法来做

---

### `CMM2可重复级Repeatable`

+ processes are characterized for specific projects and organization is often reactive
+ 有基本管理行为和设计，管理技术基于 **_相似项目_** 的 **_经验_**
+ 6个`KPA`
    1. :star2: `RM(Requirements Management, 需求管理)`
    1. `SPP(Software Project Planning, 软件项目计划)`
    1. `SPTO(Software Project Tracking and Oversight, 软件项目跟踪与监控)`
    1. `SSM(Software Subcontract Management, 软件子合同管理)`
    1. :star2: `SQA(Software Quality Assurance, 软件质量保证)`
    1. :star2:  `SCM(Software Configuration Management, 软件配置管理)`

---

### `CMM3已定义级Defined`

+ projects tailor their processes from the organization's development methodology
+ 7个`KPA`
    1. `OPF(Organization Process Focus, 组织过程焦点)`
    1. `OPD(Organization Process Definition, 组织过程定义)`
    1. `TP(Training Program, 培训程序)`
    1. `ISM(Integrated Software Management, 集成软件管理)`
    1. `SPE(Software Product Engineering, 软件产品工程)`
    1. `IC(Intergroup Coordination, 组间协调)`
    1. :star2: `PR(Peer Reviews, 同行评审)`

---

### `CMM4已管理级Managed`

+ processes are measured and controlled
+ 2个`KPA`
    1. :star2: `QPM(Quantitative Process Management, 定量过程管理)`
    1. :star2: `SQM(Software Quality Management, 软件质量管理)`

---

### `CMM5优化级Optimizing`

+ focus on process improvements
+ 3个`KPA`
    1. `DP(Defect Prevention, 缺陷预防)`
    1. `TCM(Technology Change Management, 技术变更管理)`
    1. `PCM(Process Change Management, 过程变更管理)`

---

### `PSP(Personal Software Process, 个体软件过程)`与`TSP(Team Software Process, 团队软件过程)`

---

#### `PSP`

+ `PSP`是一种可用于控制、管理和改进个人工作方式的自我持续改进过程，是一个包括软件开发表格、指南和规程的结构化框架
+ `PSP`与具体的技术（程序设计语言、工具或者设计方法）相对独立，其原则能够应用到几乎任何的软件工程任务之中
+ `PSP`能够：
    1. 说明个体软件过程的原则
    1. 帮助软件工程师做出准确的计划
    1. 确定软件工程师为改善产品质量要采取的步骤
    1. 建立度量个体软件过程改善的基准
    1. 确定过程的改变对软件工程师能力的影响

---

![height:600](./assets_image/PSP.png)

---

#### `TSP`

+ 实践证明，仅有`PSP`还是不够，因此，`CMM/SEI`又在此基础上发展出了`TSP`方法
+ `TSP`指导项目组
    1. 成员如何有效地规划和管理所面临的项目开发任务
    1. 管理人员如何指导软件开发队伍
+ `TSP`实施集体管理与自己管理自己相结合的原则，最终目的在于指导开发人员如何在最少的时间内，以预计的费用生产出高质量的软件产品。所采用的方法是对群组开发过程定义、度量和改进。

---

#### 实施`TSP`的3个先决条件

1. 高层主管和各级经理的支持，以取得必要的资源
1. 项目组开发人员已经过`PSP`的培训并有按`TSP`工作的愿望和热情
1. 整个开发组织在总体上应至少处于`CMM2`或以上，开发小组的规模以3～20人为宜

---

![height:600](./assets_image/TSP.png)

---

### `CMMI(Capacity Maturity Model Integrated, 能力成熟度集成模型)`

>1. <https://en.wikipedia.org/wiki/Capability_Maturity_Model_Integration>
>1. <https://dqsindia.com/cmmi/getting-started/>

---

+ `CMMI`是`CMM`的最新版本
+ `CMM`是一种单一的模型，一般应用于软件工程
+ `CMM`应用于软件工程外领域存在的问题
    1. 不能集中其不同过程改进的能力以取得更大成绩
    1. 要进行一些重复的培训、评估和改进活动，因而增加了许多成本
    1. 遇到不同模型中有一些对相同事物说法不一致，或活动不协调，甚至相抵触
+ 随着`CMM`的推广与模型本身的发展，该模型演绎成为一种被广泛应用的综合性模型，因此改名为`CMMI`

---

+ Originally `CMMI` addresses three areas of interest:
    1. Product and service development - CMMI for Development (`CMMI-DEV`)
    1. Service establishment, management, - CMMI for Services (`CMMI-SVC`)
    1. Product and service acquisition - CMMI for Acquisition (`CMMI-ACQ`)
+ In version 2.0 these three areas (that previously had a separate model each) were merged into a single model

---

![height:600](./assets_image/CMMI_01.png)

---

![height:400](./assets_image/CMMI_02.png)

---

![height:600](./assets_image/quality_umbrella_CMMI+TSP+PSP.jpg)

---

### `CMM`/`CMMI`中的质量保证框架

+ `SQA`是`CMM 2`中6个`KPA`之一，在`CMMI`中该`KPA`升级为管理级中的`PPQA(Process and Product Quality Assurance, 过程与产品质量保证过程)`
+ `SQA`包括评审和审计软件产品和活动，以验证它们是否符合适用的规程和标准，还包括向软件项目和其他有关的管理者提供评审和审计的结果
+ `CMM`/`CMMI`的思想，是一切从顾客需求出发，从整个组织层面上实施过程质量管理，正符合了`TQM（Total Quality Management, 全面质量管理）`的基本原则

---

![height:600](./assets_image/TQM.png)

---

+ `CMM`/`CMMI`为满足`SQA`这个关键过程域的要求需要达到以下4个目标：
    1. 软件质量保证活动是有计划的
    1. 软件产品和活动与适用的标准、规程和需求的符合性要得到客观验证
    1. 相关的小组和个人要被告知软件质量保证的活动和结果
    1. 高级管理者处理在软件项目内部不能解决的不符合问题

---

### `CMM`/`CMMI`的`SQA`的具体实施方法

1. 定义项目类型和生命周期
1. 建立`SQA`计划，确定项目审计内容
1. 生成`SQA`报告
1. 审计`SQA`报告
1. 独立汇报

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `IEEE`软件工程标准

---

+ `IEEE`软件工程标准
    1. 由`TCSE(Technical Committee on Software Engineering, 软件工程技术委员会)`的`SESS(Software Engineering Standards Subcommittee, 软件工程标准工作小组)`创建
    1. 围绕在顾客标准、资源与技术标准、流程标准、产品标准4个对象上
    1. 每个标准分为需求分析、建议惯例、指南

---

1. `IEEE 24765`
1. `IEEE 12207 SLCP(Software Life Cycle Processes)`

---

1. `IEEE 730: SQA(Software Quality Assurance)`
1. `IEEE 828: SCM(Software Configuration Management)`
1. `IEEE 29119: STD(Software Test Documentation)`
1. `IEEE 29148: SRS(Software Requirements Specification)`
1. `IEEE 1012: V&V(Software Verification and Validation)`
1. `IEEE 1016: SDD(Software Design Description)`
1. `IEEE 16326: SPM(Software Project Management)`
1. `IEEE 24748: SUD(Software User Documentation)`
1. `IEEE 1028 SRA(Software Reviews and Audits`

---

### `IEEE 730 Software Quality Assurance Plan`

><https://en.wikipedia.org/wiki/Software_quality_assurance>

<!--
FIXME: 完善730的内容
-->

---

### `IEEE 12207 Software Life Cycle Processes`

---

#### 版本历史

1. `IEEE/ISO/IEC 12207:1995`: the first iteration, published in July 1995
    1. `IEEE/ISO/IEC 12207:1995/Amd 1`:2002, an amended version of the prior, published in May 2002
    1. `IEEE/ISO/IEC 12207:1995/Amd 2`:2004, an amended version of the prior, published in November 2004
1. `IEEE Std. 12207-2008`: published in February 2008
1. `IEEE/ISO/IEC 12207-2017`: published in November 2017

---

#### 各版本分类

+ `IEEE/ISO/IEC 12207:1995`定义了17个过程，分别属于3类：
    1. `Primary Process主要过程`
    1. `Supporting Process支持过程`
    1. `Organizational Process组织过程`
+ `IEEE/ISO/IEC 12207:1995/Amd 1`定义了22个过程
+ `IEEE/ISO/IEC 12207:2008`定义了4类

---

#### `IEEE/ISO/IEC 12207:1995`

---

![height:600](./assets_image/IEEE12207_SDLC.png)

---

#### `IEEE/ISO/IEC 12207:2008`

1. 协议过程群

><http://www.ccidcom.com/xingyebaogao/20080928/B3efAuOIIM5qsys4.html>

---

>ISO/IEC/IEEE 12207:2017 divides software life cycle processes into four main process groups: agreement, organizational project-enabling, technical management, and technical processes.
>
>>[Wikipedia.ISO/IEC 12207](https://en.wikipedia.org/wiki/ISO/IEC_12207#Processes)

---

### `IEEE 1012 Software Verification and Validation`

+ `verification验证`： 用来评价某一系统或某一组件的过程，来判断给定阶段的产品是否满足该阶段开始时施加的条件
    1. 验证活动在一定的程度上是一种普通的测试活动
+ `validation确认`： 开发过程中间或结束时对某一系统或某一组件进行评价的过程，以确认它是否满足规定的需求

<!--TODO: add content of IEEE 1012-->

---

### `IEEE 1028 Software Reviews and Audits`

<!--TODO: add content of IEEE 1012-->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 其他标准

---

### `ISO/IEC 15504 Information technology - Process assessments`

1. `ISO/IEC 15504-2:2003 Part 2: Performing an assessment`
1. `ISO/IEC 15504-3:2004 Part 3: Guidance on performing an assessment`
1. `ISO/IEC 15504-4:2004 Part 4: Guidance on use for process improvement and process capability determination`

---

>ISO/IEC 15504 was initially derived from process lifecycle standard ISO/IEC 12207 and from maturity models like Bootstrap, Trillium and the Capability Maturity Model (CMM).
>
>ISO/IEC 15504 **_has been superseded by: ISO/IEC 33001:2015 Information technology - Process assessment - Concepts and terminology_** as of March, 2015 and is no longer available at ISO.  
>>[Wikipedia.ISO/IEC 15504](https://en.wikipedia.org/wiki/ISO/IEC_15504)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
