# 幻灯片Slides

## 对应书籍Textbooks

[《软件质量保证与测试（第2版）》](http://www.tup.tsinghua.edu.cn/booksCenter/book_07226401.html). 秦航、杨强 著. 清华大学出版社.2017-07. ISBN:9787302467632

## 逻辑顺序Order

1. chapter 01：概述
1. chapter 09：软件测试
1. chapter 15：测试管理
1. chapter 10：黑盒测试
1. chapter 11：白盒测试
1. chapter 08：高质量编程（静态测试的主要对象）
1. chapter 03：配置管理
1. chapter 11：软件缺陷分析 + chapter 12：基于缺陷模式的软件测试
1. chapter 13：集成测试
1. chapter 14：系统测试
小结
1. chapter 02：软件质量工程体系
1. chapter 03：软件质量度量
1. chapter 04：软件可靠性度量和测试
1. chapter 05：软件质量标准
1. chapter 06：软件评审 + chapter 08：代码审查
1. chapter 01：软件全面质量管理
