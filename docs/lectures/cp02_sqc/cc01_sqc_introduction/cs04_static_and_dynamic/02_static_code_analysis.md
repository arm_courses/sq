# 静态代码分析Static Code Analysis

## 概述

在软件开发过程中，静态代码分析往往先于动态测试之前进⾏，同时也可以作为制定动态测试 ⽤例的参考。统计证明，在整个软件开发⽣命周期中， 30% ⾄ 70% 的代码逻辑设计和编码缺 陷是可以通过静态代码分析来发现和修复的。

但是，由于静态代码分析往往要求⼤量的时间消耗和相关知识的积累，因此对于软件开发团队 来说，使⽤静态代码分析⼯具⾃动化执⾏代码检查和分析，能够极⼤地提⾼软件可靠性并节省 软件开发和测试成本。

## 参考文献Bibliographies

1. [常用 Java 静态代码分析工具的分析与比较](https://developer.ibm.com/zh/languages/java/articles/j-lo-statictest-tools/)
1. [Ensure High-Quality Android Code With Static Analysis Tools](https://code.tutsplus.com/tutorials/ensure-high-quality-android-code-with-static-analysis-tools--cms-28787)
    1. [tutsplus/ensure-high-quality-android-code-with-static-analysis-tools](https://github.com/tutsplus/ensure-high-quality-android-code-with-static-analysis-tools)
