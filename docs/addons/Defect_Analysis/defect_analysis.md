# Software Defect Analysis

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

## What is Defect

>1. a problem which, if not corrected, could cause an application to either fail or to produce incorrect results. ISO/IEC 20926:2003, Software engineering - IFPUG 4.1 Unadjusted functional size measurement method Counting practices manual
>2. an imperfection or deficiency in a project component where that component does not meet its requirements or specifications and needs to be either repaired or replaced. A Guide to the Project Management Body of Knowledge (PMBOK® Guide) - Fourth Edition.
>3. a generic term that can refer to either a fault (cause) or a failure (effect). IEEE Std 982.1-2005 IEEE Standard Dictionary of Measures of the Software Aspects of Dependability.2.1
>
>-- IEEE 24765:2010

## Types of Defect


## Levels of Defect


## Causes of Defect


## Composition of Defect
