# 测试成熟度模型Testing Maturity Model

## 概述Overview

>`TMM`最初由`IIT(Illinois Institute of Technology, 伊利诺伊理工学院)`对照`CMM`提出，Its aim to be used in a similar way to CMM, that is to provide a framework for assessing the maturity of the test processes in an organisation, and so providing targets on improving maturity.
>Each level from 2 upwards has a defined set of processes and goals, which lead to practices and sub-practices.

The TMM has been since replaced[2] by the Test Maturity Model integration and is now managed by the TMMI Foundation.[3]
>
>[Testing Maturity Model](https://en.wikipedia.org/wiki/Testing_Maturity_Model)

![TMMI model](./.assets/image/TMMi-model-picture-1-1.png)

## 参考文献Bibliographies

1. [TMMi Model](https://www.tmmi.org/tmmi-model/)
1. [Testing Maturity Model](https://en.wikipedia.org/wiki/Testing_Maturity_Model)
1. [What is the difference between debugging and testing?](https://softwareengineering.stackexchange.com/questions/79068/what-is-the-difference-between-debugging-and-testing)
1. [What is Test Maturity Model (TMM) in Software Testing?](https://www.guru99.com/what-is-test-maturity-model-tmm.html)
1. [知乎专栏：TMMi组织测试改进专栏](https://www.zhihu.com/column/c_1121737368264265728)
    1. [TMMi概述](https://zhuanlan.zhihu.com/p/74952479)
    1. [TMMi的框架结构](https://zhuanlan.zhihu.com/p/74959337)
    1. [TMMi01：TMMi成熟度级别和过程域](https://zhuanlan.zhihu.com/p/53422490)
1. [走进测试成熟度模型TMMi](https://zhuanlan.zhihu.com/p/40102899)
