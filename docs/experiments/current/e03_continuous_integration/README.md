# 持续集成Continuous Integration

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [知识储备Preparation](#知识储备preparation)
4. [思考Thinking](#思考thinking)
5. [前置条件Precondition](#前置条件precondition)
6. [原理Principles](#原理principles)
    1. [拓扑图](#拓扑图)
    2. [`DFD`](#dfd)
7. [指引Guides](#指引guides)
    1. [任务01：安装`Subversion`服务端`VisualSVN`](#任务01安装subversion服务端visualsvn)
    2. [任务02：安装`Subversion`客户端`TortoiseSVN`](#任务02安装subversion客户端tortoisesvn)
    3. [任务03：安装`Jenkins`](#任务03安装jenkins)
    4. [任务04：安装`Jenkins Plugins`](#任务04安装jenkins-plugins)
    5. [任务05：配置`Jenkins`的全局配置](#任务05配置jenkins的全局配置)
    6. [任务06：创建`Jenkins project`和`Subversion repository`](#任务06创建jenkins-project和subversion-repository)
    7. [任务07：配置`Jenkins`的项目配置集成`Subversion`、`Maven`和`email`](#任务07配置jenkins的项目配置集成subversion-maven和email)
    8. [任务08：提交代码执行`CI`](#任务08提交代码执行ci)
8. [延伸阅读Futhermore](#延伸阅读futhermore)
    1. [Jenkins](#jenkins)
    2. [Subversion](#subversion)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计性
1. 学时：6
1. 目的：
    1. 理解`CI`理念和工作流程
    1. 掌握`Jenkins`的基本使用
    1. 掌握`Jenkins`与`SCM`、`Build Tool`集成
    1. 掌握`Jenkins`的`message notification`配置
1. 内容：
    1. 任务01：安装`Subversion`服务端`VisualSVN`
    1. 任务02：安装`Subversion`客户端`TortoiseSVN`
    1. 任务03：安装`Jenkins`
    1. 任务04：安装`Jenkins Plugins`
    1. 任务05：配置`Jenkins`的全局配置
    1. 任务06：创建`Jenkins project`和`Subversion repository`
    1. 任务07：配置`Jenkins`的项目配置集成`Subversion`、`Maven`和`email`
    1. 任务08：提交代码执行`CI`
1. 环境：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Windows7 or later`, `JDK8`

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 请将指引中的 **`"e_${EN_and_FN_in_PY}"`** 修改成您的 **学号末两位** 和 **姓名全名的拼音**，如 **"e_68zhangsan"** （`e_`是前缀，不需要修改）

<!--
1. 实验内容与实验指引中各子内容包含三部分：
    1. **🚶 具体任务**
    1. **🎫 参考截图**
    1. **🖊️ 相关知识**
-->

## 知识储备Preparation

## 思考Thinking

1. `Jenkins`的运行数据保存在哪个路径下 ❓ 怎样修改保存到自定义路径下 ❓
1. `Jenkins`集成`Subversion`时双方的通讯机制有什么缺点 ❓ 更好的解决方案是什么 ❓
1. 每次`build`时未清理原来的`workspace`，用哪些方法可以达到清理的目的 ❓
1. 如何查看详细测试报告 ❓ 是否有更方便查看详细测试报告的方法 ❓

## 前置条件Precondition

1. [`Jenkins LTS 2.164.1 (2019-03-13)`](https://www.jenkins.io/changelog-stable/#v2.164.1)开始全面支持`JDK11`（同时也支持`JDK8`），预计在2022年底或2023发布的版本停止对`JDK8`的支持，因此，请使用`JDK8或以上`（建议`JDK11`）
1. `windows`下的安装包请参见[./resources/installers_win.zip](./resources/installers_win.zip)：
    1. `Jenkins LTS 2.332.3`
    1. `VisualSVN-Server-4.3.8-x64`
    1. `TortoiseSVN-1.14.3.29387-x64`
1. 源代码可使用[${repo_root}/codes/testing_sample_multi](../../../../codes/testing_sample_multi/)，也可使用任何以`maven`为构建工具的项目

## 原理Principles

### 拓扑图

![topology](./.assets/diagram/topology.png)

### `DFD`

![dfd](./.assets/diagram/dfd.png)

## 指引Guides

### 任务01：安装`Subversion`服务端`VisualSVN`

>可参考[爱代码爱编程. Windows下VisualSVN Server的安装与配置方法.](https://icode.best/i/26345535923578)

1. 按安装程序默认选项进行安装
    1. 出现`用户帐户控制`时，请选择`是`
1. 安装完成后查看`VisualSVNServer service`是否已启动（`控制面板>>>管理工具>>>服务`）

![VisualSVNServer service](./.assets/image/visualsvn_install_01.png)

### 任务02：安装`Subversion`客户端`TortoiseSVN`

>可参考[爱代码爱编程. TortoiseSVN SVN客户端安装与常用操作.](https://icode.best/i/68483544129131)

1. 按安装程序默认选项进行安装
    1. 如果出现`用户帐户控制`，请选择`"是"`

### 任务03：安装`Jenkins`

>可参考[爱代码爱编程. 基于windows操作系统安装部署jenkins教程.](https://icode.best/i/20426935923567)

❗ 以下假定已安装并配置好`JDK8`或`JDK11` ❗

1. 运行安装程序进行安装
    1. `Service Logon Credentials`请选择`Run service as LocalSystem(not recommended)`
    1. `Select Java home directory(JDK or JRE)`请确保正确的`JDK`路径被选择
    1. 如果出现`Java 11 Recommended`对话框时，点击`OK`
    1. 出现`用户帐户控制`时，请选择`是`
    1. 其他安装选项保持默认选项
1. 安装完成后查看`jenkins service`是否已启动（`控制面板>>>管理工具>>>服务`）

![jenkins install option](./.assets/image/jenkins_install_01.png)

![jenkins install option](./.assets/image/jenkins_install_02.png)

![jenkins install option](./.assets/image/jenkins_install_03.png)

![jenkins install option](./.assets/image/jenkins_install_04.png)

![jenkins install option](./.assets/image/jenkins_install_05.png)

![jenkins service status](./.assets/image/jenkins_install_06.png)

### 任务04：安装`Jenkins Plugins`

1. 使用浏览器打开`http://localhost:8080`，按提示`解锁 Jenkins`
1. `自定义Jenkins`页面，请选择`选择插件来安装`
1. 选择安装的插件：`Folders`、`Build Timeout`、`Credentials Binding`、`Timestamper`、`Workspace Cleanup`、`JUnit`、`Subversion`、`Email Extension`、`Mailer`
1. 创建管理员账户`ja_${EN_and_FN_in_PY}`（如：`ja_68zhangsan`）
1. 安装`Maven Integration`：`Manage Jenkins>>>System Configuration>>>Manage Plugins`
    1. 选择`Available`后在搜索框中输入`maven`
    1. 勾选`Maven Integration`并点击`Install without restart`
    1. 安装完成后，在浏览器输入`http://localhost:8080/restart`重启`jenkins`

![jenkins setup](./.assets/image/jenkins_setup_01.png)

![jenkins setup](./.assets/image/jenkins_setup_02.png)

![jenkins setup](./.assets/image/jenkins_setup_03.png)

![jenkins setup](./.assets/image/jenkins_setup_04.png)

![maven integration install](./.assets/image/jenkins_plugins_install_maven_01.png)

![maven integration install](./.assets/image/jenkins_plugins_install_maven_02.png)

![jenkins restart](./.assets/image/jenkins_restart.png)

### 任务05：配置`Jenkins`的全局配置

>`qq mail`的客户端授权码和`SMTP`可参考[QQMail. 客户端设置.](https://service.mail.qq.com/cgi-bin/help?id=28)

1. 配置`email`：`Manage Jenkins>>>System Configuration>>>Configure System`
    1. `System Admin e-mail address`：请输入您的邮箱地址
    1. `Extended E-mail Notification`
        1. `SMTP server`：请输入您的邮箱的`SMTP`地址
        1. `SMTP port`：请输入您的邮箱的`SMTP`端口号
        1. `Use SSL`：请根据您的邮箱是否使用`SSL`决定是否勾选
        1. `Credentials`：点击`Add`添加您的邮箱的认证信息
            1. ❗ 请注意有些邮箱（如：`qq mail`, `163 mail`）使用的密码是`客户端授权码`（不是`web`界面上的登录密码） ❗
    1. `E-mail Notification`
        1. `SMTP server`：请输入您的邮箱的`SMTP`地址
        1. `SMTP port`：请输入您的邮箱的`SMTP`端口号
        1. `Use SMTP Authentication`
            1. `用户名`：请输入您的邮箱用户名
            1. `密码`：❗ 请注意有些邮箱（如：`qq mail`, `163 mail`）使用的密码是`客户端授权码`（不是`web`界面上的登录密码） ❗
        1. `Test configuration by sending test e-mail`：进行测试是否能正确发送邮件
    1. 点击`save`保存配置信息
1. 配置`Maven`：`Manage Jenkins>>>System Configuration>>>Global Tool Configuration`
    1. `Maven>>>Maven installations`
        1. 点击`Add Maven`
        1. `Name`：输入一个自定义的名称
        1. `Install automatically`：勾选该选项，并选择`Install from Apache Version`

![jenkins setup](./.assets/image/jenkins_global_config_email_01.png)

![jenkins setup](./.assets/image/jenkins_global_config_email_02.png)

![jenkins setup](./.assets/image/jenkins_global_config_email_03.png)

![jenkins setup](./.assets/image/jenkins_global_config_email_04.png)

![jenkins setup](./.assets/image/jenkins_global_config_email_05.png)

![jenkins setup](./.assets/image/jenkins_global_config_email_06.png)

![jenkins setup](./.assets/image/jenkins_global_config_email_07.png)

![jenkins global config maven](./.assets/image/jenkins_global_config_maven_01.png)

![jenkins global config maven](./.assets/image/jenkins_global_config_maven_02.png)

### 任务06：创建`Jenkins project`和`Subversion repository`

1. 创建`jenkins project`：`NewItem`
    1. `Enter an item name`：请输入`jp_${EN_and_FN_in_PY}`（如：`jp_68zhangsan`）
    1. 选择`Freestyle project`
    1. 点击`ok`创建`jenkins project`
1. 创建`subversion repository`：运行`VisualSVN Server Manager`
    1. 在`VisualSVN Server>>>Repositories`右键点击，选择`新建>>>Repository`
    1. `Repository Type`：按默认选择
    1. `Repository Name`：请输入`sp_${EN_and_FN_in_PY}`（如：`sp_68zhangsan`）
    1. 其他按默认选择
1. 创建`subversion user`
    1. 在`VisualSVN Server>>>Users`右键点击，选择`新建>>>User`
    1. `User name`：请输入`sa_${EN_and_FN_in_PY}`（如：`sa_68zhangsan`）

![jenkins project](./.assets/image/jenkins_project_setup_01.png)

![jenkins project](./.assets/image/jenkins_project_setup_02.png)

![svn project](./.assets/image/svn_repo_setup_01.png)

![svn project](./.assets/image/svn_repo_setup_02.png)

![svn user](./.assets/image/svn_user_setup_01.png)

![svn user](./.assets/image/svn_user_setup_02.png)

### 任务07：配置`Jenkins`的项目配置集成`Subversion`、`Maven`和`email`

1. `jenkins project`配置`Subversion`
    1. `Source Code Management`
        1. 点选`Subversion`
        1. 输入`subversion repository`的地址（如：``）
    1. `Build Triggers`
        1. 勾选`Poll SCM`
        1. 在`Schedule`中输入`* * * * *`
1. `jenkins project`配置`Maven`
    1. `Build`
        1. 下拉选择`Invoke top-level Maven targets`
        1. `Maven Version`：选择已创建的`maven`
        1. `Goals`：填写`verify`
        1. 点击`Advanced...`
        1. `POM`：填写`pom.xml`
1. `jenkins project`配置`email notification`
    1. `Post-build Actions`
        1. 下拉选择`Editable Email Notification`
        1. `Project Recipient List`：填写要接受通知的邮箱地址
        1. 点击`Advanced Settings...`
        1. 点击`Add Trigger`选择`Always`

❗ 以上配置过程请及时点击`Save`保存配置 ❗

![jenkins project subversion](./.assets/image/jenkins_project_svn_01.png)

![jenkins project subversion](./.assets/image/jenkins_project_svn_02.png)

![jenkins project subversion](./.assets/image/jenkins_project_svn_03.png)

![jenkins project subversion](./.assets/image/jenkins_project_svn_04.png)

![jenkins project subversion](./.assets/image/jenkins_project_svn_05.png)

![jenkins project maven](./.assets/image/jenkins_project_maven_01.png)

![jenkins project maven](./.assets/image/jenkins_project_maven_02.png)

![jenkins project maven](./.assets/image/jenkins_project_maven_03.png)

![jenkins project email](./.assets/image/jenkins_project_email_notification_01.png)

![jenkins project email](./.assets/image/jenkins_project_email_notification_02.png)

![jenkins project email](./.assets/image/jenkins_project_email_notification_03.png)

![jenkins project email](./.assets/image/jenkins_project_email_notification_04.png)

### 任务08：提交代码执行`CI`

1. 本地`checkout remote repository`
    1. 本地新建文件夹`lws_${EN_and_FN_in_PY}`（如：`lws_68zhangsan`）
    1. 在`lws_68zhangsan/`中点击右键选择点击`SVN Checkout...`
    1. `URL of repository:`中输入`svn repository`地址
    1. 在弹出的`Authentication`对话框中输入账号密码
1. 本地`commit`
    1. 将代码复制到文件夹中，并在文件夹空白处点击右键选择`SVN Commit...`
    1. `commit`全部代码
    1. 等候约1分钟，`jenkins project`将进行`build`
    1. `build`完成后，邮箱将收到`notification email`
1. 本地`modify and commit`
    1. 对本地`src`做一些修改后重新`commit`触发新的`build`

![local checkout](./.assets/image/local_checkout_01.png)

![local checkout](./.assets/image/local_checkout_02.png)

![local checkout](./.assets/image/local_checkout_03.png)

![local checkout](./.assets/image/local_checkout_04.png)

![local commit](./.assets/image/local_commit_01.png)

![local commit](./.assets/image/local_commit_02.png)

![local commit](./.assets/image/local_commit_03.png)

![local commit](./.assets/image/local_commit_04.png)

![local update and commit](./.assets/image/local_update_commit_01.png)

![local update and commit](./.assets/image/local_update_commit_02.png)

![local update and commit](./.assets/image/local_update_commit_03.png)

## 延伸阅读Futhermore

### Jenkins

1. `Jenkins Official`
    1. [Jenkins. Installing Jenkins in Windows.](https://www.jenkins.io/doc/book/installing/windows/)
    1. [jenkinsci/docker. Official Jenkins Docker image Document.](https://github.com/jenkinsci/docker/blob/master/README.md)
    1. [Jenkins Plugins. Email Extension.](https://plugins.jenkins.io/email-ext/#plugin-content-system-wide-configuration)
1. [爱代码爱编程. 基于windows操作系统安装部署jenkins教程.](https://icode.best/i/20426935923567)
1. [陈南志. 使用Jenkins来自动打包和部署Maven工程【持续集成】.](https://blog.csdn.net/pucao_cug/article/details/82531681)
1. [Rishabh Mishra. How to Install and Configure Email Notifications in Jenkins.](https://netcorecloud.com/tutorials/install-and-configure-email-notifications-in-jenkins/)

### Subversion

1. `VisualSVN Server Official`
    1. [VisualSVN Server](https://www.visualsvn.com/server/)
1. `TortoiseSVN Official`
    1. [TortoiseSVN](https://tortoisesvn.net/)
1. [爱代码爱编程. Windows下VisualSVN Server的安装与配置方法.](https://icode.best/i/26345535923578)
1. [爱代码爱编程. SVN使用总结.](https://icode.best/i/48000042504380)
1. [爱代码爱编程. TortoiseSVN SVN客户端安装与常用操作.](https://icode.best/i/68483544129131)
1. [爱代码爱编程. TortoiseSVN svn客户端（Windows版本）.](https://icode.best/i/18708345610013)
