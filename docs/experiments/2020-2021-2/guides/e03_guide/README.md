# QA-E03：基于Jenkins的集成测试Integration Testing Based-On Jenkins

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验前提条件与储备知识](#实验前提条件与储备知识)
3. [实验注意事项](#实验注意事项)
4. [实验指引](#实验指引)
    1. [实验资源](#实验资源)
    2. [实验原理](#实验原理)
    3. [实验拓扑图](#实验拓扑图)
    4. [实验数据流图](#实验数据流图)
    5. [实验内容01](#实验内容01)
        1. [**:microscope: 实验内容01具体任务**](#microscope-实验内容01具体任务)
        2. [**:ticket: 实验内容01参考截图**](#ticket-实验内容01参考截图)
    6. [实验内容02](#实验内容02)
        1. [**:microscope: 实验内容02具体任务**](#microscope-实验内容02具体任务)
        2. [**:ticket: 实验内容02参考截图**](#ticket-实验内容02参考截图)
    7. [实验内容03](#实验内容03)
        1. [**:microscope: 实验内容03具体任务**](#microscope-实验内容03具体任务)
        2. [**:ticket: 实验内容03参考截图**](#ticket-实验内容03参考截图)
            1. [全局邮箱支持参考截图](#全局邮箱支持参考截图)
            2. [项目邮箱支持参考截图](#项目邮箱支持参考截图)
    8. [实验内容04](#实验内容04)
        1. [**:microscope: 实验内容04具体任务**](#microscope-实验内容04具体任务)
        2. [**:ticket: 实验内容04参考截图**](#ticket-实验内容04参考截图)
    9. [实验内容05](#实验内容05)
        1. [**:microscope: 实验内容05具体任务**](#microscope-实验内容05具体任务)
        2. [**:ticket: 实验内容05参考截图**](#ticket-实验内容05参考截图)
    10. [实验内容06](#实验内容06)
        1. [**:microscope: 实验内容06具体任务**](#microscope-实验内容06具体任务)
        2. [**:ticket: 实验内容06参考截图**](#ticket-实验内容06参考截图)
    11. [实验内容07](#实验内容07)
        1. [**:microscope: 实验内容07具体任务**](#microscope-实验内容07具体任务)
        2. [**:ticket: 实验内容07参考截图**](#ticket-实验内容07参考截图)
    12. [实验内容08](#实验内容08)
        1. [**:microscope: 实验内容08具体任务**](#microscope-实验内容08具体任务)
        2. [**:ticket: 实验内容08参考截图**](#ticket-实验内容08参考截图)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：设计性
1. 实验学时：4
1. 实验目的：
    1. 掌握`持续集成（Continuous Integration, CI）`的相关概念与知识
    1. 理解`Jenkins`的使用方法
1. 实验内容与要求：
    1. 实验内容01： 安装`Jenkins`及`Jenkins Plugin`
    1. 实验内容02： 在`Jenkins`中新建`project`并运行构建
    1. 实验内容03： 配置`Jenkins`的邮件支持
    1. 实验内容04： 安装`VisualSVN-Server`和`TortoiseSVN`
    1. 实验内容05： 配置`project`的`SVN`环境、配置`Jenkins`的`SVN`集成并运行构建
    1. 实验内容06： 安装`Ant`
    1. 实验内容07： 配置`Jenkins`的`Ant`集成并运行构建
    1. 实验内容08： 配置`Jenkins`的`CheckStyle`、`FindBugs`、`PMD`、`Violations`集成并运行构建
1. 实验条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Eclipse`或`Intellij IDEA`

## 实验前提条件与储备知识

1. 掌握`Java`基本编程
1. 了解`SCM(Source Control Management)`基本概念和使用方法
1. 了解`Static Code Analysis`基本概念和使用方法

## 实验注意事项

1. 实验指引中使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”
1. 实验内容与实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体任务** ==> 实验的具体任务及步骤
    1. **:ticket: 实验内容参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验指引

### 实验资源

1. installers && plugins
    1. `jenkins_lts_1.642.4` && `jenkins1.642.4 plugins`
    1. `apache-ant-1.9.7`
    1. `VisualSVN-Server-4.3.0`
    1. `TortoiseSVN-1.10.5.28651`
1. source code: `jeesite`
1. ant build script: `build1.xml` && `build2.xml`
1. email account

### 实验原理

1. 在一台`PC`上同时模拟`server`和`client`，`email server`使用公共邮箱代替（即不单独配置`email server`）：
    1. `jenkins server`： 安装部署`Jenkins`、`Ant`，设置代码目录为`<some_places>\workspace\`（`<some_places>`指磁盘上任意文件夹）
    1. `SVN server`： 安装部署`VisualSVN-Server`
    1. `client`/`SVN client`： 安装`TortoiseSVN`，设置代码目录为`<some_places>\local_snv_ws\`（`<some_places>`指磁盘上任意文件夹）
1. `jenkins server`集成`EMail`、`SVN`、`Ant`、`Static Code Analysis`（`CheckStyle`、`FindBugs`、`PMD`、`Violations`）
1. 在该台`PC`上作为`client`模拟`CI`： 代码提交至`SVN Server`，触发`build`和`test`

### 实验拓扑图

![](./assets_diagram/topology.svg)

### 实验数据流图

![](./assets_diagram/dfd.svg)

### 实验内容01

#### **:microscope: 实验内容01具体任务**

1. 安装`Jenkins`： 参照《实验教材》安装`Jenkins`
1. 启动`Jenkins`（<http://localhost:8080/>）
1. 安装`Jenkins Plugin`
    + `Jenkins 1.X`请使用离线安装方式，需先安装目标插件的依赖插件后才能安装目标插件，安装顺序如下：
        1. `email-ext`: `token-macro-1.12.1.hpi` --> `email-ext.hpi`
        1. `subversion`: `mapdb-api.hpi` -> `scm-api.hpi` -> `structs.hpi` -> `workflow-step-api.hpi` -> `workflow-scm-step.hpi` -> `subversion.hpi`
        1. `ant`
        1. `checkstyle`: `analysis-core.hpi` -> `checkstyle.hpi`
        1. `findbugs`: `jfreechart-plugin.hpi` -> `findbugs.hpi`
        1. `pmd`: `pmd.hpi`
        1. `violations`: `subversion.hpi`
<!--    + `Jenkins 2.X`请使用在线安装方式，分别安装`email-ext`、`subversion`、`ant`、`checkstyle`、`findbugs`、`pmd`、`violations`-->

:warning: :warning: :warning:

+ `Jenkins 1.X`如遇提示可升级`plugin`，请勿升级
+ `Jenkins 2.X`中`checkstyle`、`findbugs`、`pmd`已失效，其功能已合并到`Warnings Next Generation`，本实验请使用`Jenkins 1.X`

<!--1. `Jenkins 2.X`中`checkstyle`、`findbugs`、`pmd`已失效，其功能已合并到`Warnings Next Generation`
    + [`PMD Plugin`](https://wiki.jenkins.io/pages/viewpage.action?pageId=17235992): <https://wiki.jenkins.io/pages/viewpage.action?pageId=17235992>
    + [`Warnings Next Generation`](https://plugins.jenkins.io/warnings-ng/): <https://plugins.jenkins.io/warnings-ng/>-->

:warning: :warning: :warning:

#### **:ticket: 实验内容01参考截图**

### 实验内容02

#### **:microscope: 实验内容02具体任务**

1. 参照新建`<EN_and_FN_in_PY>_jeesite`项目（:warning:请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）

#### **:ticket: 实验内容02参考截图**

![](./assets_image/jenkins_project_jeesite.png)

### 实验内容03

#### **:microscope: 实验内容03具体任务**

1. **配置`jenkins server`邮箱支持（全局邮箱支持）**：使用一个现有的公共邮件账号配置`jenkins`的邮箱支持
    1. `系统配置 >>> 系统设置 >>> Jenkins Location`： 配置`系统管理员邮件地址`为现有的邮件账号
    1. `系统配置 >>> 系统设置 >>> 邮件通知`： 配置现有的邮件账号的`SMTP`信息
    1. `系统配置 >>> 系统设置 >>> Extended E-mail Notification`： 配置现有的邮件账号的`SMTP`信息
1. **配置`Project`的邮件通知（项目邮箱支持）**
    1. `<project> >>> 配置 >>> 构建后操作 >>> 增加构建后操作步骤 >>> Editable Email Notification`： 配置`Project Recipient List`, `Default Subject`, `Default Content`, `Advanced Settings >>> Triggers`

#### **:ticket: 实验内容03参考截图**

##### 全局邮箱支持参考截图

![](./assets_image/email_admin_email_addr.png)

![](./assets_image/email_notification.png)

![](./assets_image/email_test_email.png)

![](./assets_image/email_test_email_ret.png)

##### 项目邮箱支持参考截图

![](./assets_image/email_proj_editable_01.png)

![](./assets_image/email_proj_editable_02.png)

### 实验内容04

#### **:microscope: 实验内容04具体任务**

1. 按默认选项安装`VisualSVN-Server`（安装免费授权即可）
1. 按默认选项安装`TortoiseSVN`

#### **:ticket: 实验内容04参考截图**

暂无

### 实验内容05

#### **:microscope: 实验内容05具体任务**

1. **`VisualSVN Server`新建仓库**：`VisualSVN Server Manager >>> Repositorys >>> Create New Repository`新建一个`jeesite`的仓库（新建一个用户名和密码，并将该仓库的权限分配给该用户）
1. **`client`签出仓库**：
    1. 在本地磁盘新建一个名为`local_svn_ws`的文件夹
    1. 在`local_svn_ws`中右键并选择`SVN checkout...`，`checkout`出`https://localhost/svn/jeesite`（`checkout`的过程输入刚刚新建的用户名和密码）
1. **`client`模拟开发代码**：将`jeesite-master`的内容复制到刚刚`checkout`的`jeesite`目录中
1. **推送`client`的代码到`server`**：在`jeesite`目录的空白处右键并选择`SVN commit...`进行`commit`

#### **:ticket: 实验内容05参考截图**

![](./assets_image/svn-server_create_new_repo_01.png)

![](./assets_image/svn-server_create_new_repo_02.png)

![](./assets_image/svn-server_create_new_repo_03.png)

![](./assets_image/svn_checkout.png)

![](./assets_image/svn_authentication.png)

![](./assets_image/svn_checkout_ret.png)

![](./assets_image/svn_commit.png)

![](./assets_image/svn_commit_ret.png)

### 实验内容06

#### **:microscope: 实验内容06具体任务**

按《实验教材》安装`Ant`

:exclamation: :exclamation: :exclamation: 配置完`Ant`的环境变量后需重启`Jenkins`服务以使`Jenkins`读取新设置的环境变量 :exclamation: :exclamation: :exclamation: 

#### **:ticket: 实验内容06参考截图**

暂无

### 实验内容07

#### **:microscope: 实验内容07具体任务**

1. 按《实验教材》在`Jenkins`中集成`Ant`
1. 按《实验教材》运行`Ant Build`

#### **:ticket: 实验内容07参考截图**

暂无

### 实验内容08

#### **:microscope: 实验内容08具体任务**

1. 按《实验教材》在`Jenkins`中集成`CheckStyle`、`FindBugs`、`PMD`、`Violations`
1. 远行

#### **:ticket: 实验内容08参考截图**

暂无
