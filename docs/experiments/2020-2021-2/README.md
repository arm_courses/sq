# Experiments

1. 基于`JUnit`的单元测试：`JUnit`
1. 基于`PMD`的静态测试：`PMD(Programming Mistake Detector)`, `Alibaba P3C`
1. 基于`Jenkins`的集成测试：`Jenkins`, `Apache Subversion`, `Apache Ant`, `PMD`, `Checkstyle`, `Findbugs`
1. 基于`Fitnesse`的验收测试：`Fitnesse`
1. `Fitnesse`的解析与二次开发：`Fitnesse`, `Apache Gradle`
